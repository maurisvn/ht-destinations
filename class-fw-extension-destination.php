<?php
// @codingStandardsIgnoreStart
if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

class FW_Extension_Destination extends FW_Extension{
    /*POST TYPE*/
	private $post_type      = 'ht_dest';
	private $post_type_slug = 'destination';
    
    /*CATEGORY*/
    private $category_name = 'ht_dest_cat';
    private $category_slug = 'destination-category';

    /*INIT*/
    public function _init(){
        $this->ht_define_slugs();

        add_action( 'init', array( $this, 'ht_action_register_post_type' ) );
        add_action( 'init', array( $this, 'ht_action_register_category' ) );

        $this->ht_add_options();

        if ( is_admin() ) {
			$this->save_permalink_structure();
			$this->add_admin_actions();
		}
    }

    private function add_admin_actions() {
		add_action( 'admin_init', array( $this, 'destination_permalink_section' ) );
	}

	public function destination_permalink_section() {
	    add_settings_field(
	        'goto_destination_base',
	        esc_html__( 'Destination Base', 'goto' ),
	        array( $this, 'destination_base_input_field' ),
	        'permalink',
	        'optional'
	    ); 

	    add_settings_field(
	        'goto_destination_category_base',
	        esc_html__( 'Destination Category Base', 'goto' ),
	        array( $this, 'destination_category_base_input_field' ),
	        'permalink',
	        'optional'
	    );
	}

	public function destination_base_input_field() {
	    ?>
	    <input type="text" name="goto_destination_base" value="<?php echo esc_attr( $this->post_type_slug ); ?>" />
	    <code>/your-destination-permalink</code>
	    <?php
	}

	public function destination_category_base_input_field() {
	    ?>
	    <input type="text" name="goto_destination_category_base" value="<?php echo esc_attr( $this->category_slug ); ?>" />
	    <code>/your-destination-category-permalink</code>
	    <?php
	}

    /*DEFINE SLUGS*/
    private function ht_define_slugs(){
        $this->post_type_slug = apply_filters(
			'goto_destination_base_slug',
			$this->get_db_data( 'permalinks/post', $this->post_type_slug )
		);

		$this->category_slug = apply_filters(
			'goto_destination_category_slug',
			$this->get_db_data( 'permalinks/taxonomy', $this->category_slug )
		);
    }

    private function save_permalink_structure() {

		if ( ! isset( $_POST['permalink_structure'] ) && ! isset( $_POST['category_base'] ) ) {
			return;
		}

		$post = FW_Request::POST( 'goto_destination_base',
			apply_filters( 'goto_destination_base_slug', $this->post_type_slug )
		);

		$taxonomy = FW_Request::POST( 'goto_destination_category_base',
			apply_filters( 'goto_destination_category_slug', $this->category_slug )
		);


		$this->set_db_data( 'permalinks/post', $post );
		$this->set_db_data( 'permalinks/taxonomy', $taxonomy );
	}

    /*ADD OPTIONS FOR DESTINATION*/
    public function ht_add_options(){
        add_filter( 'fw_post_options', array( $this, 'ht_dest_options' ), 10, 2 );
    }

    /* POST TYPE
    ***************************************************/
    public function ht_action_register_post_type(){
        $post_names = array(
            'singular' => __( 'Destination', 'haintheme' ),
            'plural'   => __( 'Destinations', 'haintheme' )
        );

        $args = array(
            'labels'             => array(
                'name'               => $post_names['plural'],
                'singular_name'      => $post_names['singular'],
                'add_new'            => sprintf( __( 'Add New %s', 'haintheme' ), $post_names['singular'] ),
                'add_new_item'       => sprintf( __( 'Add New %s', 'haintheme' ), $post_names['singular'] ),
                'edit'               => __( 'Edit', 'haintheme' ),
                'edit_item'          => sprintf( __( 'Edit %s', 'haintheme' ), $post_names['singular'] ),
                'new_item'           => sprintf( __( 'New %s', 'haintheme' ), $post_names['singular'] ),
                'all_items'          => sprintf( __( 'All %s', 'haintheme' ), $post_names['plural'] ),
                'view'               => sprintf( __( 'View %s', 'haintheme' ), $post_names['singular'] ),
                'view_item'          => sprintf( __( 'View %s', 'haintheme' ), $post_names['singular'] ),
                'search_items'       => sprintf( __( 'Search %s', 'haintheme' ), $post_names['plural'] ),
                'not_found'          => sprintf( __( 'No %s Found', 'haintheme' ), $post_names['plural'] ),
                'not_found_in_trash' => sprintf( __( 'No %s Found In Trash', 'haintheme' ), $post_names['plural'] ),
                'parent_item_colon'  => '' /* text for parent types */
            ),
            'description'        => sprintf( __( 'Create a %s item', 'haintheme' ), $post_names['singular'] ),
            'public'             => true,
            'show_ui'            => true,
            'show_in_menu'       => true,
            'show_in_nav_menus'  => true,
            'publicly_queryable' => true,
            /* queries can be performed on the front end */
            'has_archive'        => true,
            'rewrite'            => array( 'slug' => $this->post_type_slug ),
            'menu_position'      => 7,
            'show_in_nav_menus'  => true,
            'menu_icon'          => 'dashicons-admin-site',
            'hierarchical'       => false,
            'query_var'          => true,
            'show_in_rest'       => true,
            'supports'           => array(
                'title', /* Text input field to create a post title. */
                'editor',
                'excerpt',
                'comments',
                'thumbnail', /* Displays a box for featured image. */
            ),
            'capabilities'       => array(
                'edit_post'              => 'edit_pages',
                'read_post'              => 'edit_pages',
                'delete_post'            => 'edit_pages',
                'edit_posts'             => 'edit_pages',
                'edit_others_posts'      => 'edit_pages',
                'publish_posts'          => 'edit_pages',
                'read_private_posts'     => 'edit_pages',
                'read'                   => 'edit_pages',
                'delete_posts'           => 'edit_pages',
                'delete_private_posts'   => 'edit_pages',
                'delete_published_posts' => 'edit_pages',
                'delete_others_posts'    => 'edit_pages',
                'edit_private_posts'     => 'edit_pages',
                'edit_published_posts'   => 'edit_pages',
            ),
        );

        register_post_type( $this->post_type, $args );
    }

    public function get_post_type_name() {
        return $this->post_type;
    }

    /* CATEGORY
    ***************************************************/
    public function ht_action_register_category() {
        $category_names = array(
            'singular' => __( 'Destinations Category', 'haintheme' ),
            'plural'   => __( 'Destination Categories', 'haintheme' )
        );

        $args = array(
            'labels'            => array(
                'name'              => $category_names['plural'],
                'singular_name'     => $category_names['singular'],
                'add_new'           => sprintf( __( 'Add New %s', 'haintheme' ), $category_names['singular'] ),
                'add_new_item'      => sprintf( __( 'Add New %s', 'haintheme' ), $category_names['singular'] ),
                'search_items'      => sprintf( __( 'Search %s', 'haintheme' ), $category_names['plural'] ),
                'all_items'         => sprintf( __( 'All %s', 'haintheme' ), $category_names['plural'] ),
                'parent_item'       => sprintf( __( 'Parent %s', 'haintheme' ), $category_names['singular'] ),
                'parent_item_colon' => sprintf( __( 'Parent %s:', 'haintheme' ), $category_names['singular'] ),
                'edit_item'         => sprintf( __( 'Edit %s', 'haintheme' ), $category_names['singular'] ),
                'update_item'       => sprintf( __( 'Update %s', 'haintheme' ), $category_names['singular'] ),
                'add_new_item'      => sprintf( __( 'Add New %s', 'haintheme' ), $category_names['singular'] ),
                'new_item_name'     => sprintf( __( 'New %s Name', 'haintheme' ), $category_names['singular'] ),
                'menu_name'         => $category_names['plural'],
            ),
            'public'            => true,
            'hierarchical'      => true,
            'show_ui'           => true,
            'show_admin_column' => true,
            'query_var'         => true,
            'show_in_nav_menus' => true,
            'show_tagcloud'     => false,
            'rewrite'           => array( 'slug' => $this->category_slug ),
        );

        register_taxonomy( $this->category_name, $this->post_type, $args );
    }

    public function get_category_name() {
        return $this->category_name;
    }

    /* DESTINATION POST OPTIONS
    ***************************************************/
    public function ht_dest_options( $options, $post_type ){
        if( $post_type === $this->post_type ){
            $options[] = array(
                $this->get_options( 'posts/' . $post_type, $options = array() )
            );
        }

        return $options;
    }
}