<?php
// @codingStandardsIgnoreStart
if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$manifest = array();

$manifest['name']        = esc_html__( 'Destinations', 'haintheme' );
$manifest['description'] = esc_html__( 'Create custom post type Destinations', 'haintheme' );
$manifest['bitbucket']   = 'maurisvn/ht-destinations';
$manifest['author']      = 'maurisvn';
$manifest['version']     = '1.0.7';
$manifest['display']     = true;
$manifest['standalone']  = true;
$manifest['thumbnail']   = 'fa fa-globe';