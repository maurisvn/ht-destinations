<?php

if ( ! defined( 'FW' ) ) die( 'Forbidden' );

$dest_options = array(
    'tour_id' => array(
        'type'        => 'multi-select',
        'label'       => esc_html__( 'Related Tours ( Optional )', 'haintheme' ),
        'desc'        => esc_html__( 'Select which tours which are related with this destination', 'haintheme' ),
        'population'  => 'posts',
        'source'      => 'ht_tour',
        'prepopulate' => 100,
        'limit'       => 100,
    ),
);

$options = array(
    'dest_layout_box' => array(
        'title'   => esc_html__( 'Destination Customizing', 'haintheme' ),
        'type'    => 'box',
        'options' => $dest_options
    ),
);